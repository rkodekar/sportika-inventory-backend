package com.merkmod.sportika

import com.merkmod.sportika.model.*
import org.jetbrains.annotations.Nullable

data class UserResponse(val success: Int, val message: String)
data class StockRespone(val success: Int, val message: String)
data class StockAllRespone(@Nullable val stock: List<Stock>?)
data class PurchaseResponse(val  success: Int, @Nullable var purchaseId: String?)
data class SalesResponse(val success: Int, @Nullable var salesId: String?, @Nullable var salePersonId: String?)
data class SaleItemResponse(val success: Int, val message: String)
data class FullSaleResponseBusiness(@Nullable var list: List<Sales>?)
data class FullSaleResponsePerson(@Nullable var list: List<Sales>?)
data class FullPurchaseResponse(val success: Int, @Nullable var list: List<Purchase>?)
data class PurchaseItemRespone(@Nullable val success: Int, @Nullable val purchaseBody: List<PurchaseItem>?)
data class SalesItemResponses(@Nullable val salesItems: List<SalesItem>, @Nullable val stocks: List<Stock>?)
data class StockQuantityResponse(val success: Int, val quantity: String)
data class SalePersonResponse(val success: Int, val salesPersons: List<Login>?)
data class SaleUpdateRespone(val salesBalance: SalesBalance)
data class PurchaseUpdateBalance(val purchaseBalance: PurchaseBalance)