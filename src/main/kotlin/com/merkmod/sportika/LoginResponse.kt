package com.merkmod.sportika

import com.merkmod.sportika.model.Login
import org.jetbrains.annotations.Nullable

data class LoginResponse(val success: Int, val message: String, @Nullable val login: Login?)