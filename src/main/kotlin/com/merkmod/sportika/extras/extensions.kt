package com.merkmod.sportika.extras



fun<T> T.isNotNull() = this != null