package com.merkmod.sportika

import com.fasterxml.jackson.databind.ObjectMapper
import com.merkmod.sportika.extras.isNotNull
import com.merkmod.sportika.model.*
import com.merkmod.sportika.repositories.*
import com.merkmod.sportika.requestbodies.BusinessBody
import com.merkmod.sportika.requestbodies.ItemBody
import com.merkmod.sportika.services.EmailService
import org.jetbrains.annotations.Nullable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.net.InetAddress
import java.nio.file.Files
import java.nio.file.Paths
import java.sql.Timestamp
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*
import javax.validation.Valid


@RestController
@RequestMapping("/api/v1")
class MainController(private val businesRepository: BusinessRepository,
                     private val emailVerificationRepo: EmailVerificationRepository,
                     private val stockReposirtory: StockReposirtory,
                     private val loginRespository: LoginRespository,
                     private val purchaseItemRepository: PurchaseItemRepository,
                     private val purchaseRepository: PurchaseRepository,
                     private val salesRespository: SalesRepository,
                     private val salesItemRepository: SalesItemRepository,
                     private val salesBalanceRepository: SalesBalanceRepository,
                     private val purchaseBalanceRepository: PurchaseBalanceRepository) {

    @Autowired
    lateinit var emailService: EmailService

    @Autowired
    lateinit var emailSender: JavaMailSender

    val mapper = ObjectMapper()


    var rad = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~!@#&*()"

    @GetMapping("/hello")
    fun getHello() = "hello"

    @GetMapping("/sendemail")
    fun sendEmail() {
        try {
            emailService.sendEmail(emailSender, "mkodekar@zoho.com", "123456", "55555555")
        } catch (e: IOException) {
            println(e.message)
        }
    }


    @PostMapping("/register")
    fun businessRegister(@Valid @RequestBody businessBody: BusinessBody, result: BindingResult): ResponseEntity<UserResponse> {
        return if (!result.hasErrors()) {
            val emailBusiness = businesRepository.findByBusinessEmail(businessBody.businessEmail!!)
            if (emailBusiness.isNotNull()) {
                val userResponse = UserResponse(0, "Business with email address  ${emailBusiness.businessEmail} already exists")
                ResponseEntity.ok(userResponse)
            } else {
                val businessName = businessBody.businessName
                val businessEmail = businessBody.businessEmail
                var passWord = ""
                for (i in 0..9) {
                    passWord += rad[Math.floor(Math.random() * rad.length).toInt()]
                }
                val address = businessBody.address
                val telephone = businessBody.telephone
                val ownerName = businessBody.ownerName
                val gstnumber = businessBody.gstNumber
                val ownerNumber = businessBody.ownerMobile
                val ownerId = ownerName?.take(3) + (Random().nextInt(900) + 100)
                val business_id = businessName?.take(3) + (Random().nextInt(900) + 100)
                val business = Business(
                        businessName = businessName!!,
                        businessEmail = businessEmail!!,
                        address = address!!,
                        officeNumber = telephone!!,
                        ownerName = ownerName!!,
                        password = passWord,
                        ownerId = ownerId,
                        verified = "No",
                        businessId = business_id,
                        ownerMobileNumber = ownerNumber!!,
                        gstNumber = gstnumber!!)
                businesRepository.saveAndFlush(business)
                val verificationCode = Random().nextInt(900000) + 100000
                emailService.sendEmail(emailSender, businessEmail, verificationCode.toString(), passWord)
                emailVerificationRepo.saveAndFlush(EmailVerification(verificationCode = verificationCode.toString(), businessId = business_id))
                val userResponse = UserResponse(1, "Successfully Registered")
                ResponseEntity.ok(userResponse)
            }
        } else {
            val userResponse = UserResponse(0, "Sorry not working")
            ResponseEntity.ok(userResponse)
        }
    }


    @PutMapping("/verification")
    fun verifyBusiness(@RequestParam("code") code: String, @RequestParam("business_id") businessId: String): ResponseEntity<UserResponse> {
        val data = emailVerificationRepo.findByBusinessIdAndVerificationCode(businessId, code)
        return if (data.isNotNull()) {
            businesRepository.updateVerification(businessId, "Yes")
            emailVerificationRepo.delete(data)
            val userResponse = UserResponse(1, "User successfully verified")
            ResponseEntity.ok(userResponse)
        } else {
            val userResponse = UserResponse(0, "$code or $businessId does not match")
            ResponseEntity.ok(userResponse)
        }
    }


    @RequestMapping(method = [RequestMethod.GET], value = ["/stock"])
    fun getStock(@RequestParam("businessId") businessId: String): ResponseEntity<StockAllRespone> = ResponseEntity.ok(StockAllRespone(stockReposirtory.getAllStockByBusinessId(businessId)))


    @RequestMapping(method = [RequestMethod.GET], value = ["/login"])
    fun login(@RequestParam("userName") username: String,
              @RequestParam("passWord") password: String): ResponseEntity<LoginResponse> {
        val login = loginRespository.findDesignationByUserNameAndPassword(username, password)
        return if (login.isNotNull()) {
            val loginResponse = LoginResponse(1, "Login successful", login)
            ResponseEntity.ok(loginResponse)
        } else {
            val loginResponse = LoginResponse(success = 0, message = "Login unsuccessful for $username please check you password", login = null)
            ResponseEntity.ok(loginResponse)
        }
    }

    @RequestMapping(method = [RequestMethod.GET], value = ["/getSalesPerson"])
    fun getSalesPerson(@RequestParam("username") username: String): ResponseEntity<SalePersonResponse> {
        val salesPersons = loginRespository.findUserNameByReportingPerson(username)
        return if (salesPersons.isNotEmpty()) {
            val salesPersonResponse = SalePersonResponse(1, salesPersons)
            ResponseEntity.ok(salesPersonResponse)
        } else {
            val saleResponsePerson = SalePersonResponse(0, emptyList())
            ResponseEntity.ok(saleResponsePerson)
        }

    }


    @RequestMapping(method = [RequestMethod.POST],
            produces = [MediaType.APPLICATION_JSON_VALUE],
            value = ["/updatePurchaseBalance"])
    fun updateBalancePurchase(
            @RequestParam("purchaseId") purchaseId: String,
            @RequestParam("paid") paidamount: Int,
            @RequestParam("salesManId") salesManId: String,
            @RequestParam("paymentType") paymentType: String,
            @RequestParam("image", required = false) image: MultipartFile?): ResponseEntity<PurchaseUpdateBalance> {
        val imageDirectory = "/var/www/html/images"
        return if (image.isNotNull()) {
            return try {
                val bytes = image?.bytes
                val path = Paths.get(imageDirectory + image?.originalFilename)
                Files.write(path, bytes)
                val imageUrl = "http://${InetAddress.getLocalHost().hostName}/images/${image?.originalFilename}"
                val previousPurchase = purchaseRepository.getPurchaseByPurchaseId(purchaseId)
                val purchase = purchaseRepository.updatePurchaseBalance(paidamount, purchaseId)
                val purchaseBalance = PurchaseBalance(purchaseId = purchaseId,
                        previousBalance = previousPurchase.balance,
                        currentBalance = purchase.balance,
                        paid = paidamount,
                        paymentType = paymentType,
                        saleManId = salesManId,
                        image = imageUrl
                )
                val purchaseUpdateBalance = PurchaseUpdateBalance(purchaseBalance)
                purchaseBalanceRepository.save(purchaseBalance)
                ResponseEntity(purchaseUpdateBalance, HttpStatus.OK)
            }catch (e: IOException) {
                ResponseEntity(null, HttpStatus.CONFLICT)
            }
        } else {
            val previousPurchase = purchaseRepository.getPurchaseByPurchaseId(purchaseId)
            val purchase = purchaseRepository.updatePurchaseBalance(paidamount, purchaseId)
            val purchaseBalance = PurchaseBalance(purchaseId = purchaseId,
                    previousBalance = previousPurchase.balance,
                    currentBalance = purchase.balance,
                    paid = paidamount,
                    paymentType = paymentType,
                    saleManId = salesManId
            )
            val purchaseUpdateBalance = PurchaseUpdateBalance(purchaseBalance)
            purchaseBalanceRepository.save(purchaseBalance)
            ResponseEntity(purchaseUpdateBalance, HttpStatus.OK)
        }
    }

    @RequestMapping(method = [RequestMethod.POST],
            produces = [MediaType.APPLICATION_JSON_VALUE],
            value = ["/updateBalanceSales"])
    fun updateBalanceSales(
            @RequestParam("buyerId") buyerId: String,
            @RequestParam("paid") paidamount: Int,
            @RequestParam("salesManId") salesManId: String,
            @RequestParam("paymentType") paymentType: String,
            @RequestParam("image", required = false) image: MultipartFile?): ResponseEntity<SaleUpdateRespone?> {
        val imageDirectory = "/var/www/html/images"
        return if (image.isNotNull()) {
            return try {
                val bytes = image?.bytes
                val path = Paths.get(imageDirectory + image?.originalFilename)
                Files.write(path, bytes)
                val imageUrl = "http://${InetAddress.getLocalHost().hostName}/images/${image?.originalFilename}"
                val previousSale = salesRespository.getSalesByBuyerId(buyerId)
                val sales = salesRespository.updateSaleBalance(paidamount, buyerId)
                val salesBalance = SalesBalance(buyerId = buyerId, previousBalance = previousSale.balance,
                        currentBalance = sales.balance, paid = paidamount, paymentType = paymentType,
                        saleManId = salesManId,image = imageUrl)
                val saleUpdateRespone = SaleUpdateRespone(salesBalance)
                salesBalanceRepository.save(salesBalance)
                ResponseEntity(saleUpdateRespone, HttpStatus.OK)
            }catch (e: IOException) {
                ResponseEntity(null, HttpStatus.CONFLICT)
            }
        } else {
            val previousSale = salesRespository.getSalesByBuyerId(buyerId)
            val sales = salesRespository.updateSaleBalance(paidamount, buyerId)
            val salesBalance = SalesBalance(buyerId = buyerId, previousBalance = previousSale.balance,
                    currentBalance = sales.balance, paid = paidamount, paymentType = paymentType,
                    saleManId = salesManId)
            val saleUpdateRespone = SaleUpdateRespone(salesBalance)
            salesBalanceRepository.save(salesBalance)
            ResponseEntity(saleUpdateRespone, HttpStatus.OK)
        }

    }

    @RequestMapping(method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE], value = ["/purchaseReport"])
    fun purchaseReport(
            @RequestParam("businessId") businessId: String,
            @RequestParam("from") from: String,
            @RequestParam("to") to: String
    ): ResponseEntity<FullPurchaseResponse> {
        return try {
            val fromDate = Timestamp.valueOf(from)
            val toDate = Timestamp.valueOf(to)
            val purchaseList = purchaseRepository.findByBusinessAndBetweenToAndFromDate(businessId, fromDate, toDate)
            return if (purchaseList.isEmpty()) {
                val fullPurchaseResponse = FullPurchaseResponse(0, emptyList())
                ResponseEntity(fullPurchaseResponse, HttpStatus.BAD_REQUEST)
            } else {
                val fullPurchaseResponse = FullPurchaseResponse(1, purchaseList)
                ResponseEntity(fullPurchaseResponse, HttpStatus.OK)
            }
        } catch (e: ParseException) {
            val fullPurchaseResponse = FullPurchaseResponse(0, emptyList())
            ResponseEntity(fullPurchaseResponse, HttpStatus.CONFLICT)
        }
    }

    @RequestMapping(
            method = [RequestMethod.GET],
            produces = [MediaType.APPLICATION_JSON_VALUE],
            value = ["/salesReport"])
    fun saleReport(@RequestParam("businessId") businessId: String,
                   @RequestParam("from") from: String,
                   @RequestParam("to") to: String): ResponseEntity<FullSaleResponseBusiness> {
        return try {
            val fromDate = Timestamp.valueOf(from)
            val toDate = Timestamp.valueOf(to)
            val salesList = salesRespository.generateSaleReport(businessId, fromDate, toDate)
            return if (salesList.isEmpty()) {
                val fullSalesRespone = FullSaleResponseBusiness(emptyList())
                ResponseEntity(fullSalesRespone, HttpStatus.BAD_REQUEST)
            } else {
                val fullSaleResponse = FullSaleResponseBusiness(salesList)
                ResponseEntity(fullSaleResponse, HttpStatus.OK)
            }
        } catch (e: ParseException) {
            val fullSaleResponseBusiness = FullSaleResponseBusiness(emptyList())
            ResponseEntity(fullSaleResponseBusiness, HttpStatus.CONFLICT)
        }
    }

    @RequestMapping(method = [RequestMethod.GET], value = ["/purchaserecords"])
    fun purchase(@RequestParam("businessId") businessId: String): ResponseEntity<FullPurchaseResponse> {
        val purchase = purchaseRepository.getAllPurchaseByBusinessId(businessId)
        return if (purchase.isNotNull()) {
            val fullPurchaseResponse = FullPurchaseResponse(1, purchase)
            purchase.forEach {
                println(it.businessId)
            }
            ResponseEntity.ok(fullPurchaseResponse)
        } else {
            val fullPurchaseResponse = FullPurchaseResponse(0, emptyList())
            ResponseEntity.ok(fullPurchaseResponse)
        }
    }

    @RequestMapping(method = [RequestMethod.GET], value = ["/purchaseItemRecords"])
    fun purchaseItems(@RequestParam("purchaseId") purchaseId: String): ResponseEntity<PurchaseItemRespone> {
        val purchaseItems = purchaseItemRepository.findPurchaseItemByPurchaseId(purchaseId)
        return if (purchaseItems.isNotNull()) {
            val purchaseItemRespone = PurchaseItemRespone(1, purchaseItems)
            ResponseEntity.ok(purchaseItemRespone)
        } else {
            ResponseEntity.ok(PurchaseItemRespone(0, emptyList()))
        }
    }

    @RequestMapping(method = [RequestMethod.GET], value = ["/salesRecord"])
    fun salesBusiness(@RequestParam("businessId") businessId: String): ResponseEntity<FullSaleResponseBusiness> = ResponseEntity.ok(FullSaleResponseBusiness(salesRespository.getAllSalesByBusinessId(businessId)))

    @RequestMapping(method = [RequestMethod.GET], value = ["/salesRecordPerson"])
    fun salePerson(@RequestParam("salesPersonId") salesPersonId: String): ResponseEntity<FullSaleResponsePerson> = ResponseEntity.ok(FullSaleResponsePerson(salesRespository.getAllBySalesPerson(salesPersonId)))


    @RequestMapping(method = [RequestMethod.POST], value = ["/addSalesItemRecord"])
    fun addsaleItem(@RequestParam("businessId") businessId: String,
                    @RequestParam("quantity") quantities: Array<Int>,
                    @RequestParam("salesId") salesId: String,
                    @RequestParam("salesManId") salesManId: String,
                    @RequestParam("stockId") stockIds: Array<String>): ResponseEntity<SaleItemResponse> {
        val salesItems = mutableListOf<SalesItem>()
        stockIds.forEachIndexed { index, stockId ->
            val quantity = quantities[index]
            stockReposirtory.removeStock(quantity, stockId)
            val salesItem = SalesItem(salesId = salesId, businessId = businessId, salesmanId = salesManId, stockId = stockId, quantity = quantity)
            salesItemRepository.saveAndFlush(salesItem)
            salesItems.add(salesItem)
        }

        return if (salesItems.isNotNull()) {
            val salesItemResponse = SaleItemResponse(1, "Order placed")
            ResponseEntity.ok(salesItemResponse)
        } else {
            val saleItemResponse = SaleItemResponse(0, "Cannot while placing order")
            ResponseEntity.ok(saleItemResponse)
        }
    }


    @RequestMapping(method = [RequestMethod.GET], value = ["/stockQuantity"])
    fun getCurrentQuantity(@RequestParam("stockId") stockId: String): ResponseEntity<StockQuantityResponse> {
        val quantity = stockReposirtory.findByStockId(stockId).quantity
        return if (quantity != 0) {
            val stockQuantityResponse = StockQuantityResponse(1, quantity.toString())
            ResponseEntity.ok(stockQuantityResponse)
        } else {
            val stockQuantityResponse = StockQuantityResponse(0, "Out of stock")
            ResponseEntity.ok(stockQuantityResponse)
        }
    }

    @RequestMapping(method = [RequestMethod.DELETE], value = ["/removeItem"])
    fun removeItem(@RequestParam("salesId") salesId: String,
                   @RequestParam("stockId") stockId: String,
                   @RequestParam("quantity") quantity: Int): ResponseEntity<StockRespone> {
        val salesItem = salesItemRepository.findBySalesIdAndStockId(salesId, stockId)
        val stock = stockReposirtory.updateStock(quantity, stockId)
        return if (stock.isNotNull() and salesItem.isNotNull()) {
            salesItemRepository.delete(salesItem)
            ResponseEntity.ok(StockRespone(1, "Successfully updated stock and salesItem"))
        } else {
            ResponseEntity.ok(StockRespone(0, "error while deleting salesItem "))
        }
    }

    @RequestMapping(method = [RequestMethod.GET], value = ["/salesItemRecord"])
    fun salesRecordBase(@RequestParam("salesId") salesId: String): ResponseEntity<SalesItemResponses> {
        val salesItems = salesItemRepository.getAllBySalesId(salesId)
        val stocks = salesItems.map { stockReposirtory.findByStockId(it.stockId) }
        return if (salesItems.isNotEmpty() and stocks.isNotNull()) {
            val response = SalesItemResponses(salesItems = salesItems, stocks = stocks)
            ResponseEntity.ok(response)
        } else {
            val response = SalesItemResponses(salesItems = emptyList(), stocks = emptyList())
            ResponseEntity.ok(response)
        }
    }

    @RequestMapping(method = [RequestMethod.GET], value = ["/salesItemPerson"])
    fun salesRecordPerson(@RequestParam("salesmanId") salesmaneId: String): ResponseEntity<SalesItemResponses> {
        val salesItems = salesItemRepository.getAllBySalesmanId(salesmaneId)
        val stocks = salesItems.map { stockReposirtory.findByStockId(it.stockId) }

        return if (salesItems.isNotEmpty() and stocks.isNotNull()) {
            val response = SalesItemResponses(salesItems = salesItems, stocks = stocks)
            ResponseEntity.ok(response)
        } else {
            val response = SalesItemResponses(salesItems = emptyList(), stocks = emptyList())
            ResponseEntity.ok(response)

        }
    }

    @RequestMapping(method = [RequestMethod.POST], value = ["/addSales"])
    fun addSales(@RequestParam("businessId") businessId: String,
                 @RequestParam("buyerName") buyername: String,
                 @RequestParam("salepersonId") salesPersonId: String,
                 @RequestParam("buyerAddress") address: String,
                 @RequestParam("pickupAddress") pickupAddress: String,
                 @RequestParam("tax") tax: String,
                 @RequestParam("totalAmount") totalAmount: Int,
                 @RequestParam("paid") paid: Int,
                 @RequestParam("balance") balance: Int,
                 @RequestParam("paymentType") paymentType: String,
                 @RequestParam("desc") @Nullable desc: String?,
                 @RequestParam("image", required = false) @Nullable image: MultipartFile?): ResponseEntity<SalesResponse> {
        val imageDirectory = "/var/www/html/images"
        val list = mutableListOf<Sales>()
        val salesId = buyername.take(3) + Random().nextInt((900) + 100)
        if (image.isNotNull()) {
            try {
                val bytes = image?.bytes
                val path = Paths.get(imageDirectory + image?.originalFilename)
                Files.write(path, bytes)
                val imageUrl = "http://${InetAddress.getLocalHost().hostName}/images/${image?.originalFilename}"
                println(salesId)
                if (desc.isNotNull()) {
                    val sales = Sales(
                            businessId = businessId,
                            buyerId = salesId,
                            buyerName = buyername,
                            buyerAddress = address,
                            buyerPickUpAddress = pickupAddress,
                            tax = tax,
                            salesPerson = salesPersonId,
                            totalAmount = totalAmount,
                            paid = paid,
                            balance = balance,
                            paymentType = paymentType,
                            image = imageUrl,
                            descNote = desc!!)
                    list.add(sales)
                    salesRespository.saveAndFlush(sales)
                } else {
                    val sales = Sales(
                            businessId = businessId,
                            salesPerson = salesPersonId,
                            buyerId = salesId,
                            buyerName = buyername,
                            buyerAddress = address,
                            buyerPickUpAddress = pickupAddress,
                            tax = tax,
                            totalAmount = totalAmount,
                            paid = paid,
                            balance = balance,
                            paymentType = paymentType,
                            image = imageUrl)
                    list.add(sales)
                    salesRespository.saveAndFlush(sales)
                }
            } catch (e: Exception) {
                println(e.message)
            }
        } else {
            if (desc.isNotNull()) {
                val sales = Sales(
                        businessId = businessId,
                        salesPerson = salesPersonId,
                        buyerId = salesId,
                        buyerName = buyername,
                        buyerAddress = address,
                        buyerPickUpAddress = pickupAddress,
                        tax = tax,
                        totalAmount = totalAmount,
                        paid = paid,
                        balance = balance,
                        paymentType = paymentType,
                        descNote = desc!!)
                list.add(sales)
                salesRespository.saveAndFlush(sales)
            } else {
                val sales = Sales(
                        businessId = businessId,
                        salesPerson = salesPersonId,
                        buyerId = salesId,
                        buyerName = buyername,
                        buyerAddress = address,
                        buyerPickUpAddress = pickupAddress,
                        tax = tax,
                        totalAmount = totalAmount,
                        paid = paid,
                        balance = balance,
                        paymentType = paymentType)
                list.add(sales)
                salesRespository.saveAndFlush(sales)
            }
        }

        return if (list.isEmpty()) {
            val salesResponse = SalesResponse(0, "", "")
            ResponseEntity.ok(salesResponse)
        } else {
            val salesResponse = SalesResponse(1, list[0].buyerId, list[0].salesPerson)
            ResponseEntity.ok(salesResponse)
        }
    }

    @RequestMapping(method = [RequestMethod.POST], value = ["/addpurchase"])
    fun addPurchase(@RequestParam("sellername") sellername: String,
                    @RequestParam("selleremail") selleremail: String,
                    @RequestParam("address") addressname: String,
                    @RequestParam("paid") paid: Int,
                    @RequestParam("totalamount") totalamount: Int,
                    @RequestParam("balance") balance: Int,
                    @RequestParam("paymentType") paymentType: String,
                    @RequestParam("desc") @Nullable desc: String,
                    @RequestParam("tax") tax: String,
                    @RequestParam("businessId") businessId: String,
                    @RequestParam("image", required = false) @Nullable image: MultipartFile?): ResponseEntity<PurchaseResponse> {
        val imageDirectory = "/var/www/html/images/"
        val list = mutableListOf<Purchase>()
        val purchaseId = sellername.take(3) + Random().nextInt((900) + 100)
        if (image.isNotNull()) {
            try {
                val bytes = image?.bytes
                val path = Paths.get(imageDirectory + image?.originalFilename)
                Files.write(path, bytes)
                val imageUrl = "http://${InetAddress.getLocalHost().hostName}/images/${image?.originalFilename}"
                println(purchaseId)
                if (desc.isNotNull()) {
                    val purchase = Purchase(businessId = businessId,
                            sellerName = sellername,
                            sellerEmail = selleremail,
                            address = addressname,
                            paid = paid,
                            totalAmount = totalamount,
                            balance = balance,
                            paymentType = paymentType,
                            tax = tax,
                            image = imageUrl,
                            purchaseId = purchaseId,
                            descNote = desc)
                    purchaseRepository.saveAndFlush(purchase)
                    list.add(purchase)
                } else {
                    val purchase = Purchase(businessId = businessId,
                            sellerName = sellername,
                            sellerEmail = selleremail,
                            address = addressname,
                            paid = paid,
                            totalAmount = totalamount,
                            balance = balance,
                            paymentType = paymentType,
                            tax = tax,
                            purchaseId = purchaseId,
                            image = imageUrl)
                    purchaseRepository.saveAndFlush(purchase)
                    list.add(purchase)
                }
            } catch (e: IOException) {
                println(e.message)
            }
        } else {
            if (desc.isNotNull()) {
                val purchase = Purchase(businessId = businessId,
                        sellerName = sellername,
                        sellerEmail = selleremail,
                        address = addressname,
                        paid = paid,
                        totalAmount = totalamount,
                        balance = balance,
                        purchaseId = purchaseId,
                        paymentType = paymentType,
                        tax = tax,
                        descNote = desc)
                purchaseRepository.saveAndFlush(purchase)
                list.add(purchase)
            } else {
                val purchase = Purchase(businessId = businessId,
                        sellerName = sellername,
                        sellerEmail = selleremail,
                        address = addressname,
                        paid = paid,
                        totalAmount = totalamount,
                        purchaseId = purchaseId,
                        balance = balance,
                        paymentType = paymentType,
                        tax = tax)
                purchaseRepository.saveAndFlush(purchase)
                list.add(purchase)
            }
        }

        return if (list.isEmpty()) {
            val response = PurchaseResponse(0, "")
            ResponseEntity.ok(response)
        } else {
            val response = PurchaseResponse(1, list[0].purchaseId)
            ResponseEntity.ok(response)
        }
    }

    @RequestMapping(method = [RequestMethod.POST], value = ["/addpurchaseItem"])
    fun addPurchaseItem(@RequestParam("businessId") businessId: String,
                        @RequestParam("productName") productNames: Array<String>,
                        @RequestParam("stockId") stockIds: Array<String>,
                        @RequestParam("brandName") brandNames: Array<String>,
                        @RequestParam("flavor") flavors: Array<String>,
                        @RequestParam("weight") weights: Array<String>,
                        @RequestParam("quantity") quantities: Array<String>,
                        @RequestParam("unitPrice") unitPrices: Array<String>,
                        @RequestParam("salePrice") salesPrices: Array<String>,
                        @RequestParam("purchaseId") purchaseId: String,
                        @RequestParam("image", required = false) @Nullable images: Array<MultipartFile?>?): ResponseEntity<StockRespone> {
        val imageDirectory = "/var/www/html/images/"
        val stocks = mutableListOf<Stock>()
        val purchases = mutableListOf<PurchaseItem>()
        productNames.forEachIndexed { index, productName ->
            val stockId = stockIds[index]
            val quantity = quantities[index]
            val stockExists = stockReposirtory.findByStockId(stockId)
            if (stockExists.isNotNull()) {
                stockReposirtory.updateStock(quantity.toInt(), stockId)
                stocks.add(stockExists)
                val purchaseItem = PurchaseItem(businessId = businessId, purchaseId = purchaseId, stockId = stockId, quantity = quantity.toInt())
                purchaseItemRepository.saveAndFlush(purchaseItem)
                purchases.add(purchaseItem)
            } else {
                val brandName = brandNames[index]
                val flavor = flavors[index]
                val weight = weights[index]
                val unitPrice = unitPrices[index]
                val salesPrice = salesPrices[index]
                val multipartFile = images?.get(index)
                if (multipartFile != null) {
                    try {
                        val bytes = multipartFile.bytes
                        val path = Paths.get(imageDirectory + multipartFile.originalFilename)
                        Files.write(path, bytes)
                        val imageUrl = "http://${InetAddress.getLocalHost().hostName}/images/${multipartFile.originalFilename}"
                        val purchaseItem = PurchaseItem(purchaseId, businessId, stockId, quantity.toInt())
                        val stock = Stock(businessId, productName, stockId, brandName, flavor, weight, quantity.toInt(),
                                unitPrice.toInt(), salesPrice.toInt())
                        stock.productImage = imageUrl
                        purchaseItem.stock = listOf(stock)
                        purchaseItemRepository.saveAndFlush(purchaseItem)
                        purchases.add(purchaseItem)
                    } catch (e: IOException) {
                        println(e.message)
                    }
                } else {
                    val purchaseItem = PurchaseItem(purchaseId, businessId, stockId, quantity.toInt())
                    val stock = Stock(businessId, productName, stockId, brandName, flavor, weight, quantity.toInt(),
                            unitPrice.toInt(), salesPrice.toInt())
                    purchaseItem.stock = listOf(stock)
                    purchaseItemRepository.saveAndFlush(purchaseItem)
                    purchases.add(purchaseItem)
                }
            }
        }


        return if (purchases.isEmpty()) {
            val response = StockRespone(0, "Updating stock failed")
            ResponseEntity.ok(response)
        } else {
            val response = StockRespone(1, "Successfully updated stock")
            ResponseEntity.ok(response)
        }
    }
}