package com.merkmod.sportika.services

import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import java.io.IOException


@Service
class EmailService {


    @Throws(IOException::class)
    fun sendEmail(emailSender: JavaMailSender, email: String, verificationCode: String, password: String) {
        val message = emailSender.createMimeMessage()
        val helper = MimeMessageHelper(message, true)
        val emailBody = """<html><body>Thank you for registering with Our inventory management system <br>
            |please verify by adding this password to the 6 digit number at the screen where it is asked for<br>
            |$verificationCode and password is $password
            |<body></html>
        """.trimMargin()
        helper.setTo(email)
        helper.setFrom("mkodekar@zoho.com")
        helper.setText(emailBody, false)
        helper.setSubject("Inventory management verification")
        emailSender.send(message)
    }
}