package com.merkmod.sportika.requestbodies

import org.hibernate.validator.constraints.NotBlank

data class BusinessBody(
		
	@get: NotBlank
	var businessName: String? = null,
	
	@get: NotBlank
	var businessEmail: String? = null,

	@get: NotBlank
	var address: String? = null,
	
	@get: NotBlank
	var telephone: String? = null,
	
	@get: NotBlank
	var ownerName: String? = null,

	@get: NotBlank
	var ownerMobile: String? = null,
	
	@get: NotBlank
	var gstNumber: String? = null
)
