package com.merkmod.sportika.requestbodies

import java.time.Instant
import java.util.*

data class ItemBody(

        var purchaseId: String = "",

        var businessId: String = "",

        var stockId: String = "",

        var quantity: Int = 0,

        var date: String = ""
)