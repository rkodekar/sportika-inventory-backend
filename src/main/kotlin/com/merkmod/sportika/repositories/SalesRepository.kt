package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.Sales
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.Date
import javax.transaction.Transactional

interface SalesRepository: JpaRepository<Sales, Long> {

    fun getAllSalesByBusinessId(businessId: String): List<Sales>

    fun getAllBySalesPerson(salesPersonId: String): List<Sales>

    fun getSalesByBuyerId(buyerId: String): Sales

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("Update Sales s SET s.balance = s.balance - :paid, s.paid = s.paid + :paid WHERE s.buyerId = :buyerId")
    fun updateSaleBalance(@Param("paid") paid: Int, @Param("buyerId") buyerId: String): Sales

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("SELECT s FROM Sales s WHERE s.businessId = :businessId AND s.date BETWEEN :from AND :to")
    fun generateSaleReport(
            @Param("businessId") businessId: String,
            @Param("from") from: Date,
            @Param("to") to: Date
    ): List<Sales>

}