package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.SalesItem
import org.springframework.data.jpa.repository.JpaRepository

interface SalesItemRepository: JpaRepository<SalesItem, Long> {

    fun getAllBySalesmanId(salesmanId: String): List<SalesItem>

    fun getAllBySalesId(salesId: String): List<SalesItem>

    fun findBySalesIdAndStockId(salesId: String, stockId: String): SalesItem

}