package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.Stock
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.transaction.annotation.Transactional

interface StockReposirtory: JpaRepository<Stock,  Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("SELECT s FROM Stock s WHERE s.businessId = :businessId")
    fun getAllStockByBusinessId(@Param("businessId") businessId: String): ArrayList<Stock>

    fun findByStockId(stockId: String): Stock

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("Update Stock s SET s.quantity = s.quantity + :purchaseQuantity WHERE s.stockId = :stockId")
    fun updateStock(@Param("purchaseQuantity") purchaseQuantity: Int, @Param("stockId") stockId: String)

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("Update Stock s SET s.quantity = s.quantity - :purchaseQuantity WHERE s.stockId = :stockId")
    fun removeStock(@Param("purchaseQuantity") purchaseQuantity: Int, @Param("stockId") stockId: String)

}