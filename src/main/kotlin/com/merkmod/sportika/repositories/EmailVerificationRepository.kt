package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.EmailVerification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface EmailVerificationRepository: JpaRepository<EmailVerification, Long> {
    fun findByBusinessIdAndVerificationCode(businessId: String, verificationCode: String): EmailVerification
}