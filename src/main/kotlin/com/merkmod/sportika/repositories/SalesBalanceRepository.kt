package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.SalesBalance
import org.springframework.data.jpa.repository.JpaRepository

interface SalesBalanceRepository: JpaRepository<SalesBalance, Long> {

}