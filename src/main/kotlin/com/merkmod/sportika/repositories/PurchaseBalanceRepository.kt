package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.PurchaseBalance
import org.springframework.data.jpa.repository.JpaRepository

interface PurchaseBalanceRepository: JpaRepository<PurchaseBalance, Long> {

}