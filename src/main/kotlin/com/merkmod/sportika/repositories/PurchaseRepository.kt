package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.Purchase
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.transaction.annotation.Transactional
import java.util.*

interface PurchaseRepository: JpaRepository<Purchase,Long> {

    fun findPurchaseByBusinessId(businessId: String): Purchase


    fun getAllPurchaseByBusinessId(businessId: String): List<Purchase>

    fun getPurchaseByPurchaseId(purchaseId: String): Purchase

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("Update Purchase p SET p.balance = p.balance - :paid,  p.paid = p.paid + :paid WHERE p.purchaseId = :purchaseId")
    fun updatePurchaseBalance(@Param("paid") paid: Int, @Param("purchaseId") purchaseId: String): Purchase

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("SELECT p FROM Purchase p WHERE p.businessId = :businessId AND p.date BETWEEN :from AND :to")
    fun findByBusinessAndBetweenToAndFromDate(
            @Param("businessId") businessId: String,
            @Param("from") from: Date,
            @Param("to") to: Date

    ): List<Purchase>



}