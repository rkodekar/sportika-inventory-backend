package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.Login
import org.springframework.data.jpa.repository.JpaRepository

interface LoginRespository: JpaRepository<Login, Long> {

    fun findDesignationByUserNameAndPassword(username: String, password: String): Login

    fun findUserNameByReportingPerson(username: String): List<Login>
}