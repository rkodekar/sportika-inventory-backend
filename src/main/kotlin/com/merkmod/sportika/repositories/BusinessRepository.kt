package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.Business
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface BusinessRepository: JpaRepository<Business, Long> {

    fun findByBusinessEmail(businessEmail: String): Business

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Business b SET b.verified = :verified WHERE b.businessId = :businessId")
    fun updateVerification(@Param("businessId") businessId: String, @Param("verified") verified: String)
}