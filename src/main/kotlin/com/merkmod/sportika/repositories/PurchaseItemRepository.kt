package com.merkmod.sportika.repositories

import com.merkmod.sportika.model.PurchaseItem
import org.springframework.data.jpa.repository.JpaRepository

interface PurchaseItemRepository: JpaRepository<PurchaseItem, Long> {
    fun findPurchaseItemByDate(date: String): PurchaseItem
    fun findPurchaseItemByPurchaseId(purchaseId: String): List<PurchaseItem>
}