package com.merkmod.sportika.model

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name="purchase_item")
class PurchaseItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0

    var purchaseId: String = ""

    var businessId: String = ""

    var stockId: String = ""

    var quantity: Int = 0

    @Column(unique = false, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    var date: Date = Date.from(Instant.now())

    @OneToMany(fetch = FetchType.LAZY,
            cascade = [CascadeType.ALL],
            orphanRemoval = true)
    var stock: List<Stock>? = null

    constructor()

    constructor(purchaseId: String, businessId: String, stockId: String, quantity: Int) {
        this.purchaseId = purchaseId
        this.businessId = businessId
        this.stockId = stockId
        this.quantity = quantity
    }

}