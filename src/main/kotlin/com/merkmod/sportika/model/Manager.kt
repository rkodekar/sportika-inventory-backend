package com.merkmod.sportika.model

import org.hibernate.validator.constraints.NotBlank
import javax.persistence.*


@Entity
data class Manager(

        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        @Column(unique = true)
        val businessId: String = "",

        @Column(unique = true)
        val  managerId: String = "",

        @get: NotBlank
        val managerName: String = "",

        @Column(unique = true)
        val managerEmail: String = "",

        @Column(unique = true)
        val password: String = "",

        @Column(unique = true)
        val managerMobileNumber: String = ""
)