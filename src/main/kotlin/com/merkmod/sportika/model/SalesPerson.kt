package com.merkmod.sportika.model

import org.hibernate.validator.constraints.NotBlank
import javax.persistence.*


@Entity
data class SalesPerson(

        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        @Column(unique = true)
        val businessId: String = "",

        @Column(unique = true)
        val managerId: String = "",

        @Column(unique = true)
        val salesPersonId: String = "",

        @get: NotBlank
        val salesPersonName: String = "",

        @get: NotBlank
        val salesPersonEmail: String = "",

        @Column(unique = true)
        val password: String = "",

        @get: NotBlank
        val salesPersonMobile: String = ""
)