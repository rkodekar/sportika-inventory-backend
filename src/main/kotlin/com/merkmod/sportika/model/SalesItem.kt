package com.merkmod.sportika.model

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name="sales_item")
class SalesItem {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0

    var salesId: String = ""

    var businessId: String = ""

    var salesmanId: String = ""

    var stockId: String = ""


    var quantity: Int = 0


    @Column(unique = false, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    var date: Date = Date.from(Instant.now())


    constructor()

    constructor(salesId: String, businessId: String, salesmanId: String, stockId: String, quantity: Int) {
        this.salesId = salesId
        this.businessId = businessId
        this.salesmanId = salesmanId
        this.stockId = stockId
        this.quantity = quantity
    }


}