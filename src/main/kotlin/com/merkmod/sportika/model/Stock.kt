package com.merkmod.sportika.model

import org.hibernate.validator.constraints.NotBlank
import javax.persistence.*

@Entity
@Table(name = "stock")
class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0

    @Column(unique = false)
    var businessId: String = ""

    @get: NotBlank
    var productName: String = ""

    @Column(unique = false)
    var stockId: String = ""

    @get: NotBlank
    var brandName: String = ""

    @get: NotBlank
    var flavor: String = ""

    @get: NotBlank
    var weight: String = ""

    var quantity: Int = 0

    var unitPrice: Int = 0

    var salesPrice: Int = 0

    var productImage: String = ""


    constructor()

    constructor(businessId: String, productName: String, stockId: String, brandName: String, flavor: String, weight: String,
                quantity: Int, unitPrice: Int, salesPrice: Int) {
        this.businessId = businessId
        this.productName = productName
        this.stockId = stockId
        this.brandName = brandName
        this.flavor = flavor
        this.weight = weight
        this.quantity = quantity
        this.unitPrice = unitPrice
        this.salesPrice = salesPrice
    }
}