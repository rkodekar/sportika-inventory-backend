package com.merkmod.sportika.model

import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.validator.constraints.NotBlank
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "purchase")
data class Purchase(

        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        val businessId: String = "",

        val sellerName: String = "",

        val sellerEmail: String = "",

        @get: NotBlank
        val address: String = "",

        @Column(unique = true)
        val purchaseId: String = "",

        @Column(unique = false, columnDefinition = "DATETIME")
        @Temporal(TemporalType.TIMESTAMP)
        @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
        val date: Date = Date.from(Instant.now()),

        val paid: Int = 0,

        val totalAmount: Int = 0,

        val tax: String = "",

        val balance: Int = 0,

        @get: NotBlank
        val paymentType: String = "",

        val image: String = "",

        val descNote: String = ""
)