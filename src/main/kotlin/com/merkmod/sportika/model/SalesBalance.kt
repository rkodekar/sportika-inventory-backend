package com.merkmod.sportika.model

import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.validator.constraints.NotBlank
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
data class SalesBalance(

        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        @Column(unique = false)
        val buyerId: String = "",

        val previousBalance: Int = 0,

        val currentBalance: Int = 0,

        val paid: Int = 0,

        @Column(unique = false, columnDefinition = "DATETIME")
        @Temporal(TemporalType.TIMESTAMP)
        @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
        val date: Date = Date.from(Instant.now()),

        @get: NotBlank
        val paymentType: String = "",

        val image: String? = null,

        val saleManId: String = ""

)