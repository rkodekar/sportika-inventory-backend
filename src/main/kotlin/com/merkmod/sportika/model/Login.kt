package com.merkmod.sportika.model

import javax.persistence.*

@Entity
data class Login(

        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        @Column(unique = true)
        val userName: String = "",

        @Column(unique = true)
        val password: String = "",

        val designation: String = "",

        val reportingPerson: String? = null
)