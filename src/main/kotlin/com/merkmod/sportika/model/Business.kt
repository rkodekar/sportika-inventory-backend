package com.merkmod.sportika.model

import org.hibernate.validator.constraints.NotBlank
import javax.persistence.*

@Entity
data class Business(
		@Id @GeneratedValue(strategy = GenerationType.AUTO)
		val id: Long = 0,

		@get: NotBlank
		val businessName: String = "",

		@Column(unique=true)
		val businessEmail: String = "",

		@get: NotBlank
		val address: String = "",

		@get: NotBlank
		val officeNumber: String = "",

		@get: NotBlank
		val ownerName: String = "",

		@Column(unique=true)
		val ownerId: String = "",

		@get: NotBlank
		val verified: String = "NO",

		@Column(unique=true)
		val businessId: String = "",

		@get: NotBlank
		val gstNumber: String = "",

		@Column(unique = true)
		val password: String = "",

		@get: NotBlank
		val ownerMobileNumber: String = ""
)