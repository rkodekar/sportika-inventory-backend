package com.merkmod.sportika.model

import javax.persistence.*

@Entity
data class EmailVerification(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        @Column(unique=true)
        val verificationCode: String = "",

        @Column(unique=true)
        val businessId:  String = ""
)