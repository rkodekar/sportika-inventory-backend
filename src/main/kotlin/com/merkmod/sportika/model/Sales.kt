package com.merkmod.sportika.model

import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.validator.constraints.NotBlank
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "sales")
data class Sales(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        val businessId: String = "",

        val salesPerson: String = "",

        @Column(unique = false)
        val buyerId: String = "",

        val buyerName: String = "",

        @get: NotBlank
        val buyerAddress: String = "",

        @get: NotBlank
        val buyerPickUpAddress: String = "",

        @Column(unique = false, columnDefinition = "DATETIME")
        @Temporal(TemporalType.TIMESTAMP)
        @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
        val date: Date = Date.from(Instant.now()),

        val paid: Int = 0,

        val totalAmount: Int = 0,

        val tax: String = "",

        val balance: Int = 0,

        @get: NotBlank
        val paymentType: String = "",

        val image: String = "",

        val descNote: String = ""
)