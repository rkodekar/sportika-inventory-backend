package com.merkmod.sportika

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
@EnableAutoConfiguration
class SportikaInventoryApplication

fun main(args: Array<String>) {
    SpringApplication.run(SportikaInventoryApplication::class.java, *args)
}