package com.merkmod.sportika.model

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import org.hibernate.validator.constraints.NotBlank

@Entity
data class Business(
		@Id @GeneratedValue(strategy = GenerationType.AUTO)
		val id: Long = 0,
		
		@get: NotBlank		
		val business_name: String = "",
		
		@get: NotBlank
		val business_email_id: String = "",
		
		@get: NotBlank
		val address: String = "",
		
		@get: NotBlank
		val office_number: String = "",
		
		@get: NotBlank
		val owner_name: String = "",
		
		@get: NotBlank
		val owner_id: String = "",
		
		@get: NotBlank
		val verified: String = "NO",
		
		@get: NotBlank
		val business_id: String = "",
		
		@get: NotBlank
		val gst_number: String = ""
)