package com.merkmod.sportika.repositories

import org.springframework.data.jpa.repository.JpaRepository
import com.merkmod.sportika.model.Business
import org.springframework.stereotype.Repository
import org.springframework.data.repository.CrudRepository

@Repository
interface BusinessRepository: CrudRepository<Business, Long>