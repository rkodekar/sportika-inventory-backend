package com.merkmod.sportika

data class UserResponse(val success: Int, val message: String)