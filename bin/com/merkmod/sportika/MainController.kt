package com.merkmod.sportika

import org.springframework.web.bind.annotation.RestController

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import javax.validation.Valid
import org.springframework.web.bind.annotation.RequestBody
import com.merkmod.sportika.model.Business
import org.springframework.web.bind.annotation.ResponseBody
import com.merkmod.sportika.repositories.BusinessRepository
import com.merkmod.sportika.requestbodies.BusinessBody
import org.springframework.beans.factory.annotation.Autowired
import com.merkmod.sportika.services.BusinessServices
import com.merkmod.sportika.extras.*
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.http.MediaType

@RestController
@RequestMapping("/api/v1")
class MainController(private val businesRepository: BusinessRepository) {
	
	@Autowired
	lateinit var businsessService: BusinessServices
	
	@GetMapping("/hello")
	fun getHello() = "hello"
	

	@RequestMapping(value="/register",
			method=arrayOf(RequestMethod.POST),
			consumes=arrayOf(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
	fun businessRegister(@Valid @RequestBody businessBody: BusinessBody): ResponseEntity<UserResponse> {
		val email = businsessService.getUserByEmail(businessBody.businessEmail!!, businesRepository)
		return if (email.isNotNull()) {
			val response = UserResponse(0, "User with ${businessBody.businessEmail} already exits")
			ResponseEntity.ok(response)
		} else {
			val response = UserResponse(1, "Coming soone")
			ResponseEntity.ok(response)
		}
	} 
	
}