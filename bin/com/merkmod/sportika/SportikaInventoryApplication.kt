package com.merkmod.sportika

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SportikaInventoryApplication

fun main(args: Array<String>) {
    SpringApplication.run(SportikaInventoryApplication::class.java, *args)
}